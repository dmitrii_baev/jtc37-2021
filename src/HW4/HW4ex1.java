package HW4;

import java.util.Scanner;
class HW4ex1 {
	public static int f(int n) {
		System.out.println("calculating f(" + n + ")");
		if ((n / 2) == 1) {
			System.out.println("we are going to out with: " + n);
       		return (n % 2);
    		} else {
    			return (f(n / 2) + (n % 2));
    		}
		}
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Input the number:");
		int n = scanner.nextInt();
		switch (n) {
           case  (0):
				System.out.println("No, it is not a power of 2");
				break;
           case (1):
				System.out.println("Yes, it is power of 2");;
				break;
           default:
				if (n < 0){
					System.out.println("No, it is not a power of 2");
				}else{
					int result = f (n);
					if (result > 0){
						System.out.println("No, it is not a power of 2");
					}else{
						System.out.println("Yes, it is power of 2");
					}
				}
				break;
		} 
	}
}

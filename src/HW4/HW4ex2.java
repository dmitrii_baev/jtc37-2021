package HW4;

import java.util.Scanner;
class HW4ex2 {
    public static boolean searchBin(int[] array, int x, int left, int right) {

        int center;
        System.out.println("We are searching from " + left + " to " + right + ": ");

        center = (left + right) / 2;
        if (x > array[center])
        {
            left = center + 1;
        }   
        else 
        {
            if (x < array[center])
            {
                right = center - 1;
            }
        }
        if (array[center] == x)
        {
            System.out.println("Number of required element is: " + center);
            return true;
        }        
        else
        {
            if (left <= right)
            {
                return searchBin(array, x, left, right);
            }
            else
            {
                System.out.println("There is no such element in array");
                return false;
            }
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input the number, you are looking for: ");
        int numberForSearch = scanner.nextInt();
        int numbers[] = {1, 7, 22, 28, 31, 33, 43, 55, 87, 99};
        searchBin(numbers, numberForSearch, 0, numbers.length - 1);
    }
}

package HW6;

public class Program {
    //Поле с именем
    private String name;
    //Конструктор
    public Program(String name) {
        this.name = name;
    }
    //Геттер для имени программы
    public String getProgramName() {
        return name;
    }

}

package HW6;

public class HomeWork6 {
    public static void main(String[] var0) {
        // создаем ТВ
        Tv tv = new Tv("Samsung", 10);

        //создаем каналы
        Channel channel1 = new Channel("A", 1);
        Channel channel2 = new Channel("B", 2);
        Channel channel3 = new Channel("C", 3);
        Channel channel4 = new Channel("D", 4);
        Channel channel5 = new Channel("E", 5);
        Channel channel6 = new Channel("F", 6);
        Channel channel7 = new Channel("G", 7);
        Channel channel8 = new Channel("H", 8);
        Channel channel9 = new Channel("I", 9);
        Channel channel10 = new Channel("J", 10);


        //создаем программы
        Program program1 = new Program("Good morning - 1");
        Program program2 = new Program("Good morning - 2");
        Program program3 = new Program("Good morning - 3");
        Program program4 = new Program("Good morning - 4");
        Program program5 = new Program("Good morning - 5");
        Program program6 = new Program("Good morning - 6");
        Program program7 = new Program("Good morning - 7");
        Program program8 = new Program("Good morning - 8");
        Program program9 = new Program("Good morning - 9");
        Program program10 = new Program("Good morning - 10");
        Program program11 = new Program("Good morning - 11");


        //Добавляем каналы в ТВ
        tv.letChannelIn(channel1);
        tv.letChannelIn(channel2);
        tv.letChannelIn(channel3);
        tv.letChannelIn(channel4);
        tv.letChannelIn(channel5);
        tv.letChannelIn(channel6);
        tv.letChannelIn(channel7);
        tv.letChannelIn(channel8);
        tv.letChannelIn(channel9);
        tv.letChannelIn(channel10);

        //присваиваем программы каналам
        channel1.letProgramIn(program1);
        channel1.letProgramIn(program2);
        channel2.letProgramIn(program3);
        channel2.letProgramIn(program4);
        channel3.letProgramIn(program5);
        channel3.letProgramIn(program6);
        channel4.letProgramIn(program7);
        channel4.letProgramIn(program8);
        channel5.letProgramIn(program9);
        channel5.letProgramIn(program10);
        channel6.letProgramIn(program10);
        channel6.letProgramIn(program1);
        channel7.letProgramIn(program2);
        channel7.letProgramIn(program3);
        channel8.letProgramIn(program4);
        channel8.letProgramIn(program5);
        channel9.letProgramIn(program6);
        channel9.letProgramIn(program7);
        channel10.letProgramIn(program8);
        channel10.letProgramIn(program9);
        channel1.letProgramIn(program10);

        //тут ошибка - не понимаю, в чём дело
        //ошибка error: cannot find symbol
        channel1.showPrograms();

        // создал Пульт
        RemoteController remoteController = new RemoteController("Пульт1");

        //Присваиваю пульт ТВ
        tv.setRemoteController(remoteController);//ошибка тут
        remoteController.goToTv(tv);//и тут
        System.out.println("Everything is done");
    }
}
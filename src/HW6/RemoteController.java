package HW6;

public class RemoteController {
    // поля - имя
    private String name;

    // поле - ссылка на ТВ, которым будет управлять этот контролеер
    private Tv tv;

    public RemoteController(String name) {
        this.name = name;
    }

    // метод, который назначает ТВ пульту
    public void goToTv(Tv tv) {
        this.tv = tv;
        // назначаем Пульту пульт
        this.tv.setRemoteController(this);
    }

    public String getRemoteControllerName() {
        return name;
    }

}

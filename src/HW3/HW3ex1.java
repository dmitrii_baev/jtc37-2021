package HW3;

import java.util.Scanner;
import java.util.Arrays;
class HW3ex1 {	
	public static float SummOfElements(int inputarray[]){
		float arraySum = 0;
		for (int counter = 0; counter <= (inputarray.length - 1); counter++) {
			arraySum = arraySum + inputarray[counter];
		}
		return arraySum;
	}
	public static float AverageOfElements(int inputarray[]){	
		float arraySum = 0;
		for (int counter = 0; counter <= (inputarray.length - 1); counter++) {
			arraySum = arraySum + inputarray[counter];
		}
		float average = (float)arraySum / inputarray.length;
		return average;
	}
	
	public static int[] ReverseOfArray(int inputarray[]){
		int arrayReverse[] = new int[inputarray.length];
		for (int counter = 0; counter <= (inputarray.length - 1); counter++) {
			arrayReverse[inputarray.length - 1 - counter] = inputarray[counter];
		}
		return arrayReverse;
	} 
	public static int[] BubbleSortArray(int inputarray[]){
		int temp = 0;
		int bubbleSortedArray[] = new int[inputarray.length];
		for (int i = 0; i <= (inputarray.length - 1); i++){
			bubbleSortedArray[i] = inputarray[i];
		}
		for (int i = 1; i<=(bubbleSortedArray.length-1); i++) {
			for (int j = 1; j <= (bubbleSortedArray.length-1); j++){
				if (bubbleSortedArray[j - 1] > bubbleSortedArray[j]){
					temp=bubbleSortedArray[j - 1];
					bubbleSortedArray[j - 1] = bubbleSortedArray[j];
					bubbleSortedArray[j] = temp;
				}	
			}
			
		}
		return bubbleSortedArray;
	}
	public static int[] MinSwapMax(int inputarray[]){
		int minMaxSwapedArray[] = new int[inputarray.length];
		for (int i = 0; i <= (inputarray.length - 1); i++){
			minMaxSwapedArray[i] = inputarray[i];
		}
		int minValue = minMaxSwapedArray[0];
		int maxValue = minMaxSwapedArray[0];
		int minPosition = 0;
		int maxPosition = 0;
		for (int i = 1; i <= (inputarray.length - 1); i++){
			if(minMaxSwapedArray[i] > maxValue){
				maxValue = minMaxSwapedArray[i];
				maxPosition = i;
			}
			if(minMaxSwapedArray[i] < minValue){
				minValue = minMaxSwapedArray[i];
				minPosition = i;
			}
		}
		int temp = minMaxSwapedArray[minPosition];
		minMaxSwapedArray[minPosition] = minMaxSwapedArray[maxPosition];
		minMaxSwapedArray[maxPosition] = temp;
		return minMaxSwapedArray;
	}	
	
	public static long NumberFromArray(int inputarray[]){
		String numberOut = "";
		for(int i=0; i<inputarray.length; i++){
			numberOut+=inputarray[i];
		}
		long number = Long.parseLong (numberOut.trim ());
		return number; 
	}
	

	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		System.out.print("Input the length of array:");
		int n = scanner.nextInt();
		int array[] = new int[n];
		for (int count=0; count<=(n-1); count++) {			
			System.out.print("Input the element #" + (count + 1) + ":");
			array[count]=scanner.nextInt();
		}			
		System.out.println("You have inputed:" + Arrays.toString(array));
		System.out.println("Reverse of your input is:" + Arrays.toString(ReverseOfArray(array)));
		System.out.println("Bubble sorted array is:" + Arrays.toString(BubbleSortArray(array)));
		System.out.println("Array with swaped Min and Max elements:" + Arrays.toString(MinSwapMax(array)));		
		System.out.println("Sum of elements is:" + SummOfElements(array));
		System.out.println("Average of elements is:" + AverageOfElements(array));
		System.out.println("Average converted to number is:" + NumberFromArray(array));
	}			
}

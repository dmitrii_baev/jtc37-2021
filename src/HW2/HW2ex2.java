package HW2;

import java.util.Scanner;
import java.util.Arrays;
class HW2ex2 {
	public static void main(String args[]) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int array[] = new int[n];
		int arrayReverse[] = new int[n];
		float arraySum = 0;
		for (int count=0; count<=(n-1); count++) {
			array[count]=scanner.nextInt();
			arrayReverse[n-1-count]=array[count];
			arraySum=arraySum+array[count];
		}		
		System.out.println(Arrays.toString(array));
		System.out.println(Arrays.toString(arrayReverse));
	}			
}
